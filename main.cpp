#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <sstream>

using namespace std;

template <typename T>
string to_string(T value)
{
	ostringstream os ;
	os << value ;
	return os.str() ;
}


void input();
void search(string xmlName);
void displayAll();
void outputXML();
void addClass();
string selectSeg();
void addSeg();

//void testingrako();
int main()
{
	int segNum;
	int choice;
	string segName;
	
	do{
		system("cls");
		system("title Student Database ");
		system("cls");
		system("color 0f");
    	cout << "Lim, Manigo, and Salazar." << endl;
    	cout << "Selected segment number is: " << segName << endl;
		cout << "1 - Input new student." << endl;
		cout << "2 - Select a segment." << endl;
		cout << "3 - Search for a student ID number." << endl;
		cout << "4 - Display all students." << endl;
		cout << "5 - Add a new segment." << endl;
		cout << "6 - Add classes to student." << endl;
		cout << "7 - Exit." << endl;
	
  	  	cout << "Please enter your choice: ";
		cin >> choice;
			switch (choice)
	{
	case 1:
		input();
		break;
	case 2:
        segName = selectSeg();
        break;
	case 3:
		search(segName);
		break;
	case 4:
		displayAll();
		break;
    case 5:
    	addSeg();
    	break;
    case 6:
    	addClass();
    	break;
	}

	system("pause");
	}while(choice!=7);



}

void input()
{
	system("cls");
	string fname;
	string mname;
	string lname;
	string filename;
	int id;
	int age;
	string newid;

	cout << "First name:";
	cin >> fname;
	cout << "Middle name:";
	cin >> mname;
	cout << "Last name:";
	cin >> lname;
	cout << "Age:";
	cin >> age;
	cout << "ID Number:";
	cin >> id;

	newid = to_string(id);
	newid.append(".txt");


	ofstream newStudent(newid.c_str());
	newStudent << fname << ' ' << mname << ' ' << lname << ' ' << age << ' ' << id << ' ' << endl;
	newStudent.close();
	ofstream dir("directory.txt", ios::app);
	dir << fname << ' ' << mname << ' ' << lname << ' ' << age << ' ' << id << ' ' << endl;
	dir.close();
//	main();
}


	void search(string xmlName) {

		system("cls");
		string fname;
		string mname;
		string lname;
		string filename;
		int id;
		int age;
		string newid;
		string segName;
		string xmlid;
		string studClasses;
		string cid, cname, csched, cdays;

		cout << "Segment name is: " << xmlName << endl;
		cout << "Enter student ID:";
		cin >> id;
		newid = studClasses = to_string(id);
		studClasses.append("classes.txt");
		newid.append(".txt");
		xmlid = to_string(id);
		xmlid.append(".xml");
		ifstream student(newid.c_str());
		ifstream studentClasses(studClasses.c_str());


		student >> fname >> mname >> lname  >> age >> id;
			
			string answer;
			cout << "Is this the correct student? [y/n]" << endl;
			cout << fname << ' ' << mname << ' ' << lname << ' '  << endl;

			cin >> answer;

			if (answer == "y") {
				system("cls");

				cout << "Name: " << fname << ' ' << mname << ' ' << lname << endl;
				cout << "Age: " << age <<endl;
				cout <<"ID:" << id << endl;
			}
			
			string create;
			cout << "Create xml file using selected segment? [y/n] " << endl;
			cin >> create;
			string content;
			if(create == "y"){
					
					string finalPath = "segment/" + xmlName +".xml";

					ifstream xmlf1(finalPath.c_str());
					ifstream xmlf(finalPath.c_str());
					string xmlPath = "xml/" + xmlid;
					ofstream xmlof(xmlPath.c_str(), std::ofstream::out);
					xmlof << "<student>" << endl;
					
					
					while (xmlf >> content) {
					size_t found1 = content.find_first_of("<");
					content.erase(content.begin());
					size_t found2 = content.find_first_of(">");
					int x = content.size() - found2;
					content.erase(found2,x);
//					cout << content << endl;

					
					
					if(content=="id"){
						xmlof << "<id>" << id << "</id>" << endl;
					}else if(content=="fname"){
						xmlof << "<fname>" << fname << "</fname>" << endl;
					}else if(content=="mname"){
						xmlof << "<mname>" << mname << "</mname>" << endl;
					}else if(content=="lname"){
						xmlof << "<lname>" << lname << "</lname>" << endl;
					}else if(content=="age"){
						xmlof << "<age>" << age << "</age>" << endl;
					}
					

					
					
					}
					
					cout << "XML has been saved!" <<endl;
					if(studentClasses){

						cout << "File classes found!" <<endl;
							while(studentClasses >> cid >> cname >> csched >> cdays){
				
						
								xmlof << "<classID>" << cid << "</classID>" << endl;
							
								xmlof << "<className>" << cname << "</className>" << endl;
								
								xmlof << "<classSched>" << csched << "</classSched>" << endl;
								
								xmlof << "<classDays>" << cdays << "</classDays>" << endl;
								
							}

					}else{
						cout << " Student has no classes enrolled!" << endl;
					}
					xmlof << "</student>" << endl;
					xmlof.close();
				
			}


		system("pause");

}

	void displayAll() {

		system("cls");
		string fname;
		string mname;
		string lname;
		string filename;
		int id;
		int age;

		cout << "Students Database" << endl;
		cout << "-------------------" << endl;
		ifstream dir("directory.txt");
		while(dir >> fname >> mname >> lname >> age >> id)
		{
			cout << fname << ' ' << mname << ' ' << lname << ' ' << age << ' ' << id << ' ' << endl;
		}

		system("pause");
}

void outputXML()
{
    system("cls");
    string fname;
    string mname;
    string lname;
    string filename;
    int id;
    int age;

    ifstream dir("directory.txt");
    ofstream students("items.xml", ios::app);
    students << "<students>" << endl;
    while(dir >> fname >> mname >> lname >> age >> id)
    {
        students << "<student>" << endl;
        students << "<id>" << id << "</id>" << endl;
        students << "<fname>" << fname << "</fname>" << endl;
        students << "<mname>" << mname << "</mname>" << endl;
        students << "<lname>" << lname << "</lname>" << endl;
        students << "<age>" << age << "</age>" << endl;
        students << "</student>" << endl;
    }
    students << "</students>" << endl;

    students.close();
    dir.close();

    system("pause");
//    main();
}
string selectSeg()
{
	string ret="Segment";
	string index;
	string dirContents;
	string contents;
	ifstream segDir("segment/segDirectory.txt");
		while (segDir >> dirContents) {
			
			string filepath = "segment/" + dirContents;
			cout << dirContents <<" Contents: " <<endl;
			ifstream segment(filepath.c_str());
			while(segment >> contents){
				cout << contents <<endl;
			}

		}

	string testme;
	do{
		cout << "Select a segment" <<endl;
		cin >> index;
		testme = "segment/Segment" + index +".xml";
	}while(!ifstream(testme.c_str()));
	
	ret += index;
	cout << ret << endl;
	return ret;
}
void addSeg()
{
	
	int i=1;
	string contents;
	string filename = "Segment";
	ofstream dir("segment/segDirectory.txt", ios::app);
	ifstream ifDir("segment/segDirectory.txt");
	
	while(ifDir >> contents){
		i++;
	}
	
	string index = to_string(i);
	filename.append(index);
	filename.append(".xml");
	dir << filename << endl;
	string finalFName = "segment/" +filename;
	ofstream createF(finalFName.c_str(), ios::app);
	cout << finalFName <<endl;
	
	string choice;
	string tagname;
	createF << "<student>" << endl;
	do{
		cout << "Add an xml tagname:" << endl;
		cin >> tagname;
		createF<< "<" << tagname <<">"<< "</" << tagname << ">" <<endl;
		cout << "Add another tagname? [y/n]" <<endl;
		cin >>choice;
	}while(choice!="n");
	createF << "</student>" <<endl;
}


void addClass()
{
    system("cls");
    int id;
    string newid, cname, csched, cid,cdays;
	string fname;
	string frstname,mname,lname;
	string classFileName;
	int age,sid;
    cout << "Enter student ID: ";
    cin >> id;
	
	fname = to_string(id);
	fname.append(".txt");
	ifstream student(fname.c_str());
	
	if(student){
		
		cout << "File found!" << endl;
		
		while (student >> frstname >> mname >> lname  >> age >> sid) {

			int mnameLength = mname.size();
			int lnameLength = lname.size();
			int fnameLength = fname.size();
			int lengthTotal = fnameLength + mnameLength + lnameLength;

			string answer;
			cout << "Is this the correct student? [y/n]" << endl;
			cout << frstname << ' ' << mname << ' ' << lname << ' '  << endl;

			cin >> answer;

			if (answer == "y") {
				system("cls");

				cout << "Name: " << frstname << ' ' << mname << ' ' << lname << endl;
				cout << "Age: " << age <<endl;
				cout <<"ID:" << sid << endl;
				
				classFileName = to_string(sid);
				classFileName.append("classes.txt");
				ofstream xmlof(classFileName.c_str(), ios_base::app);

				
				cout << "Input Class ID:";
				cin >> cid;
				cout << "Input Class Name:";
				cin >> cname;
				cout << "Input Class Schedule: ";
				cin >> csched;
				cout << "Input Class Days: ";
				cin >> cdays;
				cout << cid << " " << cname << " " << csched << " " << cdays << endl;
				
				xmlof << cid << ' ' << cname << ' ' << csched << ' ' << cdays << ' ' << endl;
				xmlof.close();
				
			}
		}
	}else{
		
		cout << "Student Not Found!" <<endl;
		
	}
	
//    cout << "Enter class name: ";
//    cin >> cname;
//    cout << "Enter class schedule: ";
//    cin >> csched;
//    cout << "Enter classroom: ";
//    cin >> croom;
//
//
//    newid = to_string(id);
//    newid.append(".txt");
//
//    ofstream dir(newid.c_str(), ios::app);
//    dir << id << cname << ' ' << csched << ' ' << croom << ' ' << endl;
//    dir.close();
//
//    system("pause");
//    main();
}


// getting tag names
//ifstream student("seg1.xml");
//		while (student >> fname) {
//			size_t found1 = fname.find_first_of("<");
//			fname.erase(fname.begin());
//			size_t found2 = fname.find_first_of(">");
//			int x = fname.size() - found2;
//			fname.erase(found2,x);
//
//			cout << fname << endl;
//
//		}


// trash code before string answer;
//			cout << "Is this the correct student? [y/n]" << endl;
//			cout << fname << ' ' << mname << ' ' << lname << ' '  << endl;
//
//			cin >> answer;
//
//			if (answer == "y") {
//				system("cls");



//				cout << "name:";
//				for (int y = 1; y < lengthTotal; y++) {
//					cout << " ";
//				}
//				cout << "age";
//				for (int z = 1; z <2; z++) {
//					cout << " ";
//				}
//				cout << "id" << endl;
//				for (int x = 1; x < lengthTotal + 20; x++) {
//					cout << "-";
//				}
//				cout << ' ' << endl;



// trash code after 		student >> fname >> mname >> lname  >> age >> id;
//
//			int mnameLength = mname.size();
//			int lnameLength = lname.size();
//			int fnameLength = fname.size();
//			int lengthTotal = fnameLength + mnameLength + lnameLength;
